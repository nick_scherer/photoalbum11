package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Model for the Album that is Serializable
 * Has name and list of photos
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class Album implements Serializable {

	private static final long serialVersionUID = 473609096067308990L;
	private String name;
	private List<Photo> photos;
	
	public Album(String name) {
		this.name = name;
		photos = new ArrayList<Photo>();
	}
	
	public Album(String name, List<Photo> photos) {
		this.name = name;
		this.photos = photos;
	}

	/**
	 * 
	 * @return name of the album
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return list of photos in the album
	 */
	public List<Photo> getPhotos() {
		return photos;
	}

	/**
	 * Set the photos in the album
	 * @param photos list of photos in the album
	 */
	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}
	
	/**
	 * Set the name of the album
	 * @param name	name of the album to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Add a photo to the album
	 * @param photo		photo to add to album
	 */
	public void addPhoto(Photo photo) {
		photos.add(photo);
	}
	
	/**
	 * Remove a photo from the album
	 * @param photo		photo to remove from the album
	 */
	public void removePhoto(Photo photo) {
		photos.remove(photo);
	}
	
	/**
	 * Get amount of photos in the album
	 * @return amount of photos
	 */
	public int getAmountOfPhotos() {
		return photos.size();
	}
	
	/**
	 * Get the oldest photo in the album
	 * @return photo with the oldest date
	 */
	public Photo getOldestPhoto() {
		if(photos.size() > 0) {
			Photo oldestPhoto = photos.get(0);
			for(int i=1; i < photos.size(); i++) {
				Photo temp = photos.get(i);
				if(temp.getDate().compareTo(oldestPhoto.getDate()) < 0 ) {
					oldestPhoto = temp;
				}
			}
			return oldestPhoto;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the newest photo in the album
	 * @return photo with the newest date
	 */
	public Photo getNewestPhoto() {
		if(photos.size() > 0) {
			Photo newestPhoto = photos.get(0);
			for(int i=1; i < photos.size(); i++) {
				Photo temp = photos.get(i);
				if(temp.getDate().compareTo(newestPhoto.getDate()) > 0 ) {
					newestPhoto = temp;
				}
			}
			return newestPhoto;
		} else {
			return null;
		}
	}
	
	/**
	 * Get the range of dates in the album 
	 * @return String of date range
	 */
	public String getRange() {
		Photo oldest = getOldestPhoto();
		Photo newest = getNewestPhoto();

		String oldestDate = (oldest == null ? "N/A" : oldest.getDateString());
		String newestDate = (newest == null ? "N/A" : newest.getDateString());
		
		return oldestDate + " - " + newestDate;
	}
	
	/**
	 * Get index of a photo in the list
	 * @param photo		photo to get index of
	 * @return index of the photo
	 */
	public int indexOf(Photo photo) {
		if(photo == null) return -1;
		for(Photo p : photos) {
			if(p.getCaption().equals(photo.getCaption()) 
					&& p.getDate().equals(photo.getDate())) {
				return photos.indexOf(p);
			}
		}
		return -1;
	}
	
	/**
	 * Get a photo from the given index
	 * @param index			index of photo 
	 * @return photo at the index in the list
	 */
	public Photo getPhoto(int index) {
		return photos.get(index);
	}
	
	/**
	 * get the next photo from a index
	 * @param photoIndex	index of current photo in the list
	 * @return next photo from the index
	 */
	public Photo getNextPhoto(int photoIndex) {
		if(photoIndex + 1 == photos.size()) {
			return null;
		}
		return photos.get(photoIndex + 1);
	}
	
	/**
	 * get the previous photo from a index
	 * @param photoIndex	index of current photo in the list
	 * @return previous photo from the index
	 */
	public Photo getPrevPhoto(int photoIndex) {
		if(photoIndex - 1 < 0) {
			return null;
		}
		return photos.get(photoIndex - 1);
	}
	
	/**
	 * Convert Album object to a String
	 */
	public String toString() {
		return name;
	}
	
}
