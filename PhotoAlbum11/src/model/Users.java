package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Model for the Users that is Serializable
 * Has list of users
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class Users implements Serializable {

	private static final long serialVersionUID = 473609096067308990L;
	static String filepath = "data" + File.separator + "userData.dat";
	private List<User> users;
	
	public Users() {
		users = new ArrayList<User>();
	}
	
	/**
	 * 
	 * @return list of users
	 */
	public List<User> getList() {
		return users;
	}
	
	/**
	 * 
	 * @param userName		name of account
	 * @return User with given userName
	 */
	public User getUser(String userName) {
		if(users == null) return null;
		for(int i=0; i<users.size(); i++) {
			if(users.get(i).getUserName().equals(userName)) {
				return users.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Add a user to the list of users
	 * 
	 * @param user		user to add to list
	 */
	public void addUser(User user) {
		users.add(user);
	}
	
	/**
	 * Remove a user from the list of users
	 * 
	 * @param user		user to remove from list
	 */
	public void removeUser(User user) {
		users.remove(user);
	}
	
	/**
	 * 
	 * @param userName		name of the user
	 * @return true if list has a user with the given userName
	 */
	public boolean hasUser(String userName) {
		if(users == null) return false;
		for(int i=0; i<users.size(); i++) {
			if(users.get(i).getUserName().equals(userName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Reads list of users from userData.dat
	 * @return list of Users from file
	 */
	public static Users read() {
		Users u = null;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filepath));
			u = (Users) ois.readObject();
			ois.close();
		} catch (IOException e) {

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return u;
	}

	/**
	 * Writes list of users to userData.dat
	 * @param u			List of users
	 */
	public static void write(Users u) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filepath));
			oos.writeObject(u);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
