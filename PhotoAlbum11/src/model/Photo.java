package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Model for the Photo that is Serializable
 * Has caption, date of creation, filepath, and tags
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class Photo implements Serializable {

	private static final long serialVersionUID = 473609096067308990L;
	private String caption;
	private Calendar date;
	private String imgPath;
	private List<Tag> tags;


	public Photo(String caption, String imgPath, long time) {
		this.caption = caption;
		this.imgPath = imgPath;
		date = Calendar.getInstance();
		date.setTimeInMillis(time);
		tags = new ArrayList<Tag>();

	}
	
	/**
	 * 
	 * @return		caption of the photo
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * 
	 * @return		Calendar object of the photo
	 */
	public Calendar getDate() {
		return date;
	}
	
	/**
	 * 
	 * @return		Date of the photo in string
	 */
	public String getDateString() {
		String[] temp = date.getTime().toString().split(" ");
		return temp[1] + " " + temp[2] + " " + temp[5];
	}
	
	/**
	 * 
	 * @return		List of tags on the photo
	 */
	public List<Tag> getTags() {
		return tags;
	}
	
	/**
	 * Set the caption of the photo
	 * @param caption		caption for the photo
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * Add a tag to the photo
	 * @param tag		tag for the photo
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
	}
	
	/**
	 * Remove a tag from the photo
	 * @param tag		tag to be removed
	 */
	public void removeTag(Tag tag) {
		tags.remove(tag);
	}
	
	/**
	 * Convert Photo object to String
	 */
	public String toString() {
		return caption;
	}

	/**
	 * Get the image path
	 * @return image path location
	 */
	public String getImgPath() {
		return imgPath;
	}

	/**
	 * Set the image path 
	 * @param imgPath		location of image on disk
	 */
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	/**
	 * Get a string of the tags
	 * @return list of tags converted to a string
	 */
	public String getTagsString() {
		String tagString = "";
		for(Tag tag : tags) {
			tagString += tag.toString() + ", ";
		}
		return tagString == "" ? "none" : tagString;
	}
	
	/**
	 * Check if photo was taken between two dates
	 * @param start		Start date to check
	 * @param end		End date to check
	 * @return true if photo's date is between star and end
	 */
	public boolean inRange(LocalDate start, LocalDate end) {
		LocalDate photoDate = date.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if(photoDate.isAfter(start) && photoDate.isBefore(end)) {
			return true;
		} else if(photoDate.equals(start)) {
			return true;
		} else if(photoDate.equals(end)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Check if photo has a certain tag
	 * @param tag		tag to check if photo has
	 * @return true if photo has this tage
	 */
	public boolean hasTag(String tag) {
		tag = tag.trim();
		String[] types = tag.split(":");
		String[] values = tag.split(" ");
		String type = "", value = "";
		if(types.length == 2) {
			type = types[0];
		} 
		if(values.length == 2) {
			value = types[1];
		}
		for(Tag t : tags) {
			if(type.trim().equals(t.getType()) && value.trim().equals(t.getValue())) {
				return true;
			}
		}
		return false;
	}


}
