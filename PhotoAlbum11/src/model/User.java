package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Model for the User that is Serializable
 * Has username and list of albums
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class User implements Serializable {

	private static final long serialVersionUID = 473609096067308990L;
	private String userName;
	private List<Album> albums;
	
	public User(String userName) {
		this.userName = userName;
		albums = new ArrayList<Album>();
	}
	
	/**
	 * 
	 * @return username of the user
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * 
	 * @return list of albums the user has
	 */
	public List<Album> getAlbums() {
		return albums;
	}
	
	/**
	 * 
	 * @param albumName		name of an album
	 * @return album with the given album name
	 */
	public Album getAlbum(String albumName) {
		for(int i=0; i < albums.size(); i++) {
			if(albums.get(i).getName().equals(albumName)) {
				return albums.get(i);
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param albumName		name of an album
	 * @return true if user has that album
	 */
	public boolean hasAlbum(String albumName) {
		for(int i=0; i < albums.size(); i++) {
			if(albums.get(i).equals(albumName)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Add an album to the list of albums
	 * @param album		album to add to list
	 */
	public void addAlbum(Album album) {
		albums.add(album);
	}
	
	/**
	 * Remove album from list of albums
	 * @param album		album to remove from list
	 */
	public void removeAlbum(Album album) {
		albums.remove(album);
	}
	
	/**
	 * Convert album object to string
	 */
	public String toString() {
		return userName;
	}
	
}
