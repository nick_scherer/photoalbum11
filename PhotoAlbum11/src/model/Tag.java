package model;

import java.io.Serializable;

/**
 * Model for the Tag that is Serializable
 * Has type and value
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class Tag implements Serializable {

	private static final long serialVersionUID = 473609096067308990L;
	private String type;
	private String value;
	
	public Tag(String type, String value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * 
	 * @return tag's type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 
	 * @return tag's value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Set type of the tag
	 * @param type		Type of the tag
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Set value of the tag
	 * @param value		Value of the tag
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Convert Tag object to String
	 */
	public String toString() {
		return type + ": " + value;
	}
}
