package controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;
import model.Users;

/**
 * Controller for the photo display screen, which allows
 * for viewing of a single picture that was selected
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class PhotoController {

	@FXML ImageView imageView;

	@FXML Text captionText;
	@FXML Text albumText;
	@FXML Text dateText;
	@FXML Text tagsText;

	@FXML Button nextButton;
	@FXML Button prevButton;
	@FXML Button captionButton;
	@FXML Button tagsButton;
	@FXML Button backButton;
	@FXML Button logoutButton;

	private Album album;
	private User user;
	private Users users;
	private Photo photo;
	private int photoIndex;
	String filepath = "data" + File.separator + "userData.dat";

	/**
	 * Loads the user and photos in the album
	 * and initializes the photo view controller
	 * 
	 * @param userName	
	 * 				the name of the current user who is logged in
	 * @param albumName
	 * 				the name of the current album being viewed
	 * @param selected
	 * 				the name of the current photo being viewed
	 */
	public void initialize(String userName, String albumName, Photo selected) {
		users = Users.read();
		user = users.getUser(userName);
		album = user.getAlbum(albumName);	
		photo = selected;
		changePhoto(photo.getImgPath());
		photoIndex = album.indexOf(selected);
		captionText.setText("Caption: " + photo.getCaption());
		albumText.setText("Album: " + album);
		dateText.setText("Date: " + photo.getDateString());
		tagsText.setText("Tags: " + photo.getTagsString());
		if(photoIndex == album.getPhotos().size() - 1) {
			nextButton.setDisable(true);
		}
		if(photoIndex == 0) {
			prevButton.setDisable(true);
		}
	}
	
	/**
	 * Handles user action when the next button is clicked. 
	 * Next photo in list is shown
	 * 
	 * @param event			Button pressing event
	 */
	public void handleNextButton(ActionEvent event) {
		Photo next = album.getNextPhoto(photoIndex);
		if(next != null) {
			captionText.setText("Caption: " + next.getCaption());
			dateText.setText("Date: " + next.getDateString());
			tagsText.setText("Tags: ");		
			changePhoto(next.getImgPath());
			photoIndex++;
			if(photoIndex == album.getPhotos().size() - 1) {
				prevButton.setDisable(false);
				nextButton.setDisable(true);
			} else {
				prevButton.setDisable(false);
				nextButton.setDisable(false);
			}
		}
	}
	
	/**
	 * Handles user action when the prev button is clicked. 
	 * Previous photo in list is shown
	 * 
	 * @param event			Button pressing event
	 */
	public void handlePrevButton(ActionEvent event) {
		Photo prev = album.getPrevPhoto(photoIndex);
		if(prev != null) {
			captionText.setText("Caption: " + prev.getCaption());
			dateText.setText("Date: " + prev.getDateString());
			tagsText.setText("Tags: ");
			changePhoto(prev.getImgPath());
			photoIndex--;
			if(photoIndex == 0) {
				prevButton.setDisable(true);
				nextButton.setDisable(false);
			} else {
				prevButton.setDisable(false);
				nextButton.setDisable(false);
			}
		}
	}
	
	/**
	 * Handles user action when the caption button is clicked. 
	 * Opens caption editing dialog
	 * 
	 * @param event			Button pressing event
	 */
	public void handleCaptionButton(ActionEvent event) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Edit Caption");
		dialog.setHeaderText("Edit caption for this photo");
		dialog.setContentText("Caption: ");
		dialog.setResizable(false);
		   
		Optional<String> result = dialog.showAndWait();
		   			
		if(result.isPresent()) {
			int i = user.getAlbums().indexOf(album);
			photo.setCaption(result.get());
			album.getPhotos().set(photoIndex,photo);
			user.getAlbums().set(i,album);
			Users.write(users);
			captionText.setText("Caption: " + photo.getCaption());
		}
	}
	
	/**
	 * Handles user action when the tags button is clicked. 
	 * Opens tag editing screen
	 * 
	 * @param event			Button pressing event
	 */
	public void handleTagsButton(ActionEvent event) {
		FXMLLoader gui = new FXMLLoader();
		gui.setLocation(getClass().getResource("/view/tag.fxml"));

		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		Parent root = null;
		try {
			root = gui.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		TagController controller = gui.getController();
		controller.initialize(user.getUserName(), album.getName(), photo);

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Edit Tags");
		stage.setResizable(false);
		stage.show();
	}
	
	/**
	 * Handles user action when the back button is clicked. 
	 * Returns the user to the album screen
	 * 
	 * @param event			Button pressing event
	 */
	public void handleBackButton(ActionEvent event) {
		FXMLLoader gui = new FXMLLoader();
		gui.setLocation(getClass().getResource("/view/album.fxml"));

		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		Parent root = null;
		try {
			root = gui.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		AlbumController controller = gui.getController();
		controller.initialize(user.getUserName(), album.getName());

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle(album.getName());
		stage.setResizable(false);
		stage.show();
	}

	/**
	 * Changes the photo in the ImageView
	 * 
	 * @param filepath 		Filepath of a photo in the album
	 */
	private void changePhoto(String filepath) {
		File file = new File(filepath);
		String imagepath = null;
		try {
			imagepath = file.toURI().toURL().toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Image img = new Image(imagepath, 200, 200, false, true);
		imageView.setImage(img);

	}

}
