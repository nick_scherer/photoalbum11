package controller;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Album;
import model.User;
import model.Users;

/**
 * Controller for the user screen, which allows
 * for adding, deleting, renaming, and opening albums
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class UserController {

	@FXML ListView<Album> list;
	
	@FXML Text usernameText;

	@FXML Button addButton;
	@FXML Button deleteButton;
	@FXML Button renameButton;
	@FXML Button openButton;
	@FXML Button logoutButton;
	
	private User user;
	private Users users;
	private ObservableList<Album> albumList = FXCollections.observableArrayList();
	String filepath = "data" + File.separator + "userData.dat";
	
	/**
	 * Loads the user and photo being edited
	 * and initializes the tag editing view controller
	 * 
	 * @param userName
	 * 				the name of the current user who is logged in
	 */
	public void initialize(String userName) {
		users = Users.read();
		user = users.getUser(userName);
		usernameText.setText("User: " + userName);
		albumList = FXCollections.observableArrayList(user.getAlbums());
		list.setItems(albumList);
		list.getSelectionModel().select(0);
		
		list.setCellFactory(param -> new ListCell<Album>() {
			private ImageView imageView = new ImageView();

			@Override
			public void updateItem(Album album, boolean empty) {
				super.updateItem(album, empty);
				
				AnchorPane anchor = new AnchorPane();
				StackPane stack = new StackPane();
				
				Button openButton = new Button("Open");
				Button renameButton = new Button("Rename");
				Button deleteButton = new Button("Delete");
				deleteButton.setVisible(false);
				renameButton.setVisible(false);
				openButton.setVisible(false);
				
				Label albumName = new Label();
				Label dateRange = new Label();
				Label oldestPhoto = new Label();
				Label numberOfPhotos = new Label();							
				
				AnchorPane.setLeftAnchor(albumName, 65.0);
				AnchorPane.setTopAnchor(albumName, 0.0);				
				AnchorPane.setLeftAnchor(dateRange, 65.0);
				AnchorPane.setTopAnchor(dateRange, 15.0);
				AnchorPane.setLeftAnchor(oldestPhoto, 65.0);
				AnchorPane.setTopAnchor(oldestPhoto, 30.0);
				AnchorPane.setLeftAnchor(numberOfPhotos, 65.0);
				AnchorPane.setTopAnchor(numberOfPhotos, 45.0);	
				AnchorPane.setLeftAnchor(openButton, 250.0);
				AnchorPane.setTopAnchor(openButton, 15.0);	
				AnchorPane.setLeftAnchor(renameButton, 340.0);
				AnchorPane.setTopAnchor(renameButton, 15.0);	
				AnchorPane.setLeftAnchor(deleteButton, 450.0);
				AnchorPane.setTopAnchor(deleteButton, 15.0);				
				
				StackPane.setAlignment(imageView, Pos.CENTER);		
				imageView.setPreserveRatio(true);			
				
				anchor.getChildren().addAll(imageView, stack, openButton, renameButton, deleteButton,
						albumName, dateRange, oldestPhoto, numberOfPhotos);
				
				anchor.setPrefHeight(60.0);
				setGraphic(anchor);
				
				if (empty) {
					setText(null);
					setGraphic(null);
				} else if(album != null){			
					if(album.getAmountOfPhotos() > 0) {
						File file = new File(album.getPhoto(0).getImgPath());
						String imagepath = null;
	
						try {
							imagepath = file.toURI().toURL().toString();
						} catch (IOException e) {
							e.printStackTrace();
						}
	
						Image img = new Image(imagepath, 60, 60, false, true);
						imageView.setImage(img);
					}
					
					albumName.setText("Album: " + album.getName());
					albumName.setFont(Font.font ("Verdana", 14));
					dateRange.setText(album.getRange());
					oldestPhoto.setText("Oldest Photo: " + (album.getAmountOfPhotos() > 0 ? album.getOldestPhoto().getDateString() : "N/A"));
					numberOfPhotos.setText("Number of Photos: " + album.getAmountOfPhotos());
					deleteButton.setVisible(true);
					renameButton.setVisible(true);
					openButton.setVisible(true);
					
					deleteButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Delete Album");
							alert.setHeaderText("Delete " + album.getName());
							alert.setContentText("Are you sure you want to delete " + album.getName() + "?");
							   
							Optional<ButtonType> result = alert.showAndWait();
							   			
							if(result.get() == ButtonType.OK) {
								user.removeAlbum(album);
								albumList.remove(album);
								Users.write(users);
								list.setItems(albumList);	
							}
						}
					});
					
					renameButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {
							TextInputDialog dialog = new TextInputDialog();
							dialog.setTitle("Rename Album");
							dialog.setHeaderText("Rename " + album.getName());
							dialog.setContentText("New Album Name: ");
							dialog.setResizable(false);
							   
							Optional<String> result = dialog.showAndWait();
							   			
							if(result.isPresent()) {
								if(user.hasAlbum(result.get())) {
									Alert alert = new Alert(AlertType.ERROR);
									alert.setTitle("Album Already Exists");
									alert.setHeaderText("You already have an album named " + result.get());
									alert.setContentText("Please choose a different name");
									alert.showAndWait();
								} else {
									int i = albumList.indexOf(album);
									album.setName(result.get());
									Users.write(users);
									albumList.set(i,album);
									list.setItems(albumList);
								}
							}
						}
					});
					
					openButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {
							FXMLLoader gui = new FXMLLoader();
							gui.setLocation(getClass().getResource("/view/album.fxml"));

							Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
							Parent root = null;
							try {
								root = gui.load();
							} catch (IOException e) {
								e.printStackTrace();
							}
							
							AlbumController controller = gui.getController();
							controller.initialize(user.getUserName(), album.getName());

							Scene scene = new Scene(root);
							stage.setScene(scene);
							stage.setTitle(album.getName());
							stage.setResizable(false);
							stage.show();
						}
					});

				}
			}
		});
	}

	
	/**
	 * Handles user action when the add button is clicked. 
	 * Adds an album to the user account
	 * 
	 * @param event			Button pressing event
	 */
	public void handleAddButton(ActionEvent event) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Create Album");
		dialog.setHeaderText("Create a new album for your photos");
		dialog.setContentText("Album Name: ");
		dialog.setResizable(false);
		   
		Optional<String> result = dialog.showAndWait();
		   			
		if(result.isPresent()) {
			if(user.hasAlbum(result.get())) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Album Already Exists");
				alert.setHeaderText("You already have an album named " + result.get());
				alert.setContentText("Please create a NEW album");
				alert.showAndWait();
			} else {
				Album album = new Album(result.get());
				user.addAlbum(album);
				albumList = FXCollections.observableArrayList(user.getAlbums());
				list.setItems(albumList);
				Users.write(users);
				initialize(user.getUserName());
			}
		}

	}

	
	/**
	 * Handles user action when the logout button is clicked. 
	 * Logs the user out and returns to the login screen
	 * 
	 * @param event			Button pressing event
	 */
	public void handleLogoutButton(ActionEvent event) {
		FXMLLoader gui = new FXMLLoader();
		gui.setLocation(getClass().getResource("/view/login.fxml"));

		Stage stage = PhotoAlbum.getPrimaryStage();
		Parent root = null;
		try {
			root = gui.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Log In");
		stage.setResizable(false);
		stage.show();
	}
	
}