package controller;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.User;
import model.Users;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;

/**
 * Controller for the admin screen, which allows for
 * adding and deleting of users
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */

public class AdminController {

	@FXML ListView<User> list;
	
	@FXML Button addButton;
	@FXML Button deleteButton;
	@FXML Button logoutButton;

	private ObservableList<User> userList = FXCollections.observableArrayList();
	private Users users;
	String filepath = "data" + File.separator + "userData.dat";
	
	/**
	 * Loads the list of users and initializes the admin view controller
	 */
	public void initialize() {
		users = Users.read();
		if(users == null) users = new Users();
		userList = FXCollections.observableArrayList(users.getList());
		list.setItems(userList);
		list.getSelectionModel().select(0);
		
		if(users.getList().size() == 0) {
			deleteButton.setDisable(true);
		}
	}

	/**
	 * Handles user action when the log in button is clicked. 
	 * Lets an admin add a user to Photo Album
	 * 
	 * @param event		Button pressing event
	 */
	public void handleAddButton(ActionEvent event) {

		Button b = (Button) event.getSource();

		if (b == addButton) {
			TextInputDialog dialog = new TextInputDialog();
			dialog.setTitle("Create New User");
			dialog.setHeaderText("Add a new user to PhotoAlbum");
			dialog.setContentText("Username: ");
			dialog.setResizable(false);
			   
			Optional<String> result = dialog.showAndWait();
			   			
			if(result.isPresent()) {
				if(users.hasUser(result.get())) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("User Already Exists");
					alert.setHeaderText("User already exists in PhotoAlbum");
					alert.setContentText("Please create a NEW user");
					alert.showAndWait();
				} else {
					User user = new User(result.get());
					users.addUser(user);
					userList.add(user);
					Users.write(users);
				}
			}
		}
		deleteButton.setDisable(false);
	}

	/**
	 * Handles user action when the log in button is clicked. 
	 * Lets an admin delete a user from Photo Album
	 * 
	 * @param event 		Button pressing event
	 */
	public void handleDeleteButton(ActionEvent event) {

		Button b = (Button) event.getSource();

		if (b == deleteButton) {
			User selected = list.getSelectionModel().getSelectedItem();
			int selectedIndex = list.getSelectionModel().getSelectedIndex();
			if (selected != null) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Delete User");
				alert.setHeaderText("Delete user from PhotoAlbum");
				alert.setContentText("Are you sure you want to delete " + selected.getUserName() + "?");
				   
				Optional<ButtonType> result = alert.showAndWait();
				   			
				if(result.get() == ButtonType.OK) {
					users.removeUser(selected);
					userList.remove(selected);
					Users.write(users);
					list.setItems(userList);
					if (selectedIndex < userList.size()) {
						list.getSelectionModel().selectNext();
					}				
				}
			}
			
		}
	}
	
	/**
	 * Handles user action when the log in button is clicked. 
	 * Lets the admin log out of the admin menu 
	 * 
	 * @param event			Button pressing event
	 */
	
	public void handleLogoutButton(ActionEvent event) {
		FXMLLoader gui = new FXMLLoader();
		gui.setLocation(getClass().getResource("/view/login.fxml"));

		Stage stage = PhotoAlbum.getPrimaryStage();
		Parent root = null;
		try {
			root = gui.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle("Log In");
		stage.setResizable(false);
		stage.show();
	}

}