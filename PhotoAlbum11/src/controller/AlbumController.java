package controller;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;
import model.Users;

/**
 * Controller for the album screen, which allows
 * for adding, deleting, editing, displaying,
 * copying, and moving of photos in the album
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class AlbumController {

	@FXML ListView<Photo> list;

	@FXML Text usernameText;
	@FXML Text albumText;
	@FXML Text numberText;
	@FXML Text oldestText;
	@FXML Text rangeText;

	@FXML Button addButton;
	@FXML Button deleteButton;
	@FXML Button editButton;
	@FXML Button displayButton;
	@FXML Button copyButton;
	@FXML Button moveButton;
	@FXML Button createButton;
	@FXML Button backButton;
	@FXML Button logoutButton;

	@FXML TextField tagField;
	
	@FXML DatePicker start;
	@FXML DatePicker end;
	
	private Album album;
	private String imgPath;
	private long time;
	private User user;
	private Users users;
	private List<Photo> photos;
	private ObservableList<Photo> photoList = FXCollections.observableArrayList();
	String filepath = "data" + File.separator + "userData.dat";
	
	/**
	 * Sets the initial values of album data and model so it can work with the
	 * view element
	 *
	 * @param userName
	 *            the name of the current user who is logged in
	 * @param albumName
	 *            the name of the current album being viewed
	 * @see String
	 * @see User
	 * @see Users
	 */
	public void initialize(String userName, String albumName) {
		users = Users.read();
		user = users.getUser(userName);
		album = user.getAlbum(albumName);
		photos = album.getPhotos();
		usernameText.setText("User: " + userName);
		albumText.setText("Album: " + albumName);
		numberText.setText("Number of Photos: " + album.getAmountOfPhotos());
		oldestText.setText("Oldest Photo: " + 
				(album.getOldestPhoto() == null ? "null" : album.getOldestPhoto().getDateString()));
		rangeText.setText("Date Range: " + album.getRange());
		photoList = FXCollections.observableArrayList(album.getPhotos());
		list.setItems(photoList);
		list.getSelectionModel().select(0);
		
		list.setCellFactory(param -> new ListCell<Photo>() {
			private ImageView imageView = new ImageView();

			@Override
			public void updateItem(Photo photo, boolean empty) {
				super.updateItem(photo, empty);
				
				AnchorPane anchor = new AnchorPane();
				StackPane stack = new StackPane();
				
				Button displayButton = new Button("Display");
				Button editButton = new Button("Edit");
				Button deleteButton = new Button("Delete");
				Button moveButton = new Button("Move");
				Button copyButton = new Button("Copy");
				displayButton.setVisible(false);
				editButton.setVisible(false);
				deleteButton.setVisible(false);
				moveButton.setVisible(false);
				copyButton.setVisible(false);
				
				Label caption = new Label();
				Label date = new Label();
				Label tags = new Label();						
				
				AnchorPane.setLeftAnchor(caption, 60.0);
				AnchorPane.setTopAnchor(caption, 0.0);				
				AnchorPane.setLeftAnchor(date, 60.0);
				AnchorPane.setTopAnchor(date, 15.0);
				AnchorPane.setLeftAnchor(tags, 60.0);
				AnchorPane.setTopAnchor(tags, 30.0);
				AnchorPane.setLeftAnchor(displayButton, 0.0);
				AnchorPane.setBottomAnchor(displayButton, 0.0);	
				AnchorPane.setLeftAnchor(editButton, 75.0);
				AnchorPane.setBottomAnchor(editButton, 0.0);	
				AnchorPane.setLeftAnchor(deleteButton, 135.0);
				AnchorPane.setBottomAnchor(deleteButton, 0.0);	
				AnchorPane.setLeftAnchor(moveButton, 210.0);
				AnchorPane.setBottomAnchor(moveButton, 0.0);	
				AnchorPane.setLeftAnchor(copyButton, 280.0);
				AnchorPane.setBottomAnchor(copyButton, 0.0);				
				
				StackPane.setAlignment(imageView, Pos.CENTER);		
				imageView.setPreserveRatio(true);		
				
				anchor.getChildren().addAll(imageView, stack, caption, date, tags,
						displayButton, editButton, deleteButton, moveButton, copyButton);
				
				anchor.setPrefHeight(80.0);
				setGraphic(anchor);
				
				if (empty) {
					setText(null);
					setGraphic(null);
				} else {
					File file = new File(photo.getImgPath());
					String imagepath = null;

					try {
						imagepath = file.toURI().toURL().toString();
					} catch (IOException e) {
						e.printStackTrace();
					}

					Image img = new Image(imagepath, 50, 50, false, true);
					imageView.setImage(img);
					
					caption.setText("Caption: " + photo.getCaption());
					date.setText("Date: " + photo.getDateString());
					tags.setText("Tags: " + photo.getTagsString());
					displayButton.setVisible(true);
					editButton.setVisible(true);
					deleteButton.setVisible(true);
					moveButton.setVisible(true);
					copyButton.setVisible(true);
					
					displayButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {

							FXMLLoader gui = new FXMLLoader();
							gui.setLocation(getClass().getResource("/view/photo.fxml"));

							Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
							Parent root = null;
							try {
								root = gui.load();
							} catch (IOException e) {
								e.printStackTrace();
							}
							 
							PhotoController controller = gui.getController();
							controller.initialize(user.getUserName(), album.getName(), photo);


							Scene scene = new Scene(root);
							stage.setScene(scene);
							stage.setTitle("Photo Display");
							stage.setResizable(false);
							stage.show();
						}
					});
					
					editButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {

							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Edit Photo");
							alert.setHeaderText("What do you want to edit?");
							alert.setContentText("Choose your option.");

							ButtonType captionButton = new ButtonType("Edit Caption");
							ButtonType tagsButton = new ButtonType("Edit Tags");
							ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

							alert.getButtonTypes().setAll(captionButton, tagsButton, cancelButton);

							Optional<ButtonType> result = alert.showAndWait();
							if (result.get() == captionButton){
								TextInputDialog dialog = new TextInputDialog(photo.getCaption());
								dialog.setTitle("Edit Photo");
								dialog.setHeaderText("Edit the caption of the photo");
								dialog.setContentText("Please enter new caption:");

								Optional<String> captionResult = dialog.showAndWait();
								if (captionResult.isPresent()){
									int i = user.getAlbums().indexOf(album);
									int j = album.indexOf(photo);
									photo.setCaption(captionResult.get());
									album.getPhotos().set(j,photo);
									user.getAlbums().set(i,album);
									photoList.set(j, photo);
									list.setItems(photoList);
									Users.write(users);
								}
							} else if (result.get() == tagsButton) {
								FXMLLoader gui = new FXMLLoader();
								gui.setLocation(getClass().getResource("/view/tag.fxml"));

								Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
								Parent root = null;
								try {
									root = gui.load();
								} catch (IOException e) {
									e.printStackTrace();
								}
								 
								TagController controller = gui.getController();
								controller.initialize(user.getUserName(), album.getName(), photo);

								Scene scene = new Scene(root);
								stage.setScene(scene);
								stage.setTitle("Edit Tags");
								stage.setResizable(false);
								stage.show();
							} else {
							}
						}
					});
					
					deleteButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {
						
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Delete Photo");
							alert.setHeaderText("Delete Photo from " + album.getName());
							alert.setContentText("Are you sure you want to delete this photo?");
							   
							Optional<ButtonType> result = alert.showAndWait();
							   			
							if(result.get() == ButtonType.OK) {
								album.removePhoto(photo);
								photoList.remove(photo);
								Users.write(users);
								list.setItems(photoList);
							}
							
							if(photoList.size() == 0) {
								deleteButton.setDisable(true);
								editButton.setDisable(true);
								displayButton.setDisable(true);
								copyButton.setDisable(true);
								moveButton.setDisable(true);
								createButton.setDisable(true);
								tagField.setDisable(true);
								start.setDisable(true);
								end.setDisable(true);
							}
						}
					});
					
					moveButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {

							List<Album> choices = new ArrayList<Album>();
							for(Album a : user.getAlbums()) {
								if(!a.equals(album)) choices.add(a);
							}
							ChoiceDialog<Album> dialog = new ChoiceDialog<>(choices.get(0), choices);
							dialog.setTitle("Move Photo");
							dialog.setHeaderText("Move to Which Album");
							dialog.setContentText("Album:");
							   
							Optional<Album> result = dialog.showAndWait();
							   			
							if(result.isPresent()) {
								Album moveLocation = result.get();
								if(moveLocation.getPhotos().contains(photo)) {
									Alert alert = new Alert(AlertType.ERROR);
									alert.setTitle("Photo Already Exists");
									alert.setHeaderText("Photo already exists in " + result.get().getName());
									alert.setContentText("You can't have duplicate photos in an album.");
									alert.showAndWait();
								} else {
									moveLocation.addPhoto(photo);
									album.removePhoto(photo);
									photoList.remove(photo);
									Users.write(users);
									list.setItems(photoList);
								}
							}
						}
					});
					
					copyButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent event) {

							List<Album> choices = new ArrayList<Album>();
							for(Album a : user.getAlbums()) {
								if(!a.equals(album)) choices.add(a);
							}
							ChoiceDialog<Album> dialog = new ChoiceDialog<>(choices.get(0), choices);
							dialog.setTitle("Copy Photo");
							dialog.setHeaderText("Copy to Which Album");
							dialog.setContentText("Album:");
							   
							Optional<Album> result = dialog.showAndWait();
							   			
							if(result.isPresent()) {
								Album copyLocation = result.get();
								if(copyLocation.getPhotos().contains(photo)) {
									Alert alert = new Alert(AlertType.ERROR);
									alert.setTitle("Photo Already Exists");
									alert.setHeaderText("Photo already exists in " + result.get().getName());
									alert.setContentText("You can't have duplicate photos in an album.");
									alert.showAndWait();
								} else {
									copyLocation.addPhoto(photo);
									Users.write(users);		
								}
							}
						}
					});
				}
			}
		});

		if(photoList.size() == 0) {
			createButton.setDisable(true);
			tagField.setDisable(true);
			start.setDisable(true);
			end.setDisable(true);
		}
	}
	
	/**
	 * Handles all actions when the add photo button is clicked. Utilizes custom
	 * dialogs in order to search and find a photo as well as add the
	 * appropriate caption. Saves the data and updates the view.
	 *
	 * @param event			Button pressing event
	 * @see ActionEvent
	 * @throws IOException		IO Exception
	 * @throws URISyntaxException	URI Syntax Exception
	 */
	public void handleAddButton(ActionEvent event) throws IOException, URISyntaxException {
		Dialog<String> dialog = new Dialog<>();
		dialog.setTitle("Add Photo");
		dialog.setHeaderText("Add Photo to " + album.getName());
		dialog.setResizable(false);
		// Set the button types.

		ButtonType finishedButton = new ButtonType("OK", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(finishedButton, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		gridPane.setPadding(new Insets(20, 150, 10, 10));

		TextField caption = new TextField();
		Button chooseFile = new Button();
		caption.setPromptText("Caption");
		chooseFile.setText("Choose Image");

		chooseFile.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("View Pictures");
				fileChooser.setInitialDirectory(new File(System.getProperty("user.dir").toString() + "/data/images"));
				fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Images", "*.*"),
						new FileChooser.ExtensionFilter("JPG", "*.jpg"),
						new FileChooser.ExtensionFilter("GIF", "*.gif"),
						new FileChooser.ExtensionFilter("BMP", "*.bmp"),
						new FileChooser.ExtensionFilter("PNG", "*.png"));
				fileChooser.setTitle("Open Resource File");

				File newImg = fileChooser.showOpenDialog(PhotoAlbum.getPrimaryStage());
				if(newImg != null) {
					imgPath = newImg.getAbsolutePath();
					time = newImg.lastModified();
				}

			}
		});


		gridPane.add(caption, 0, 0);
		gridPane.add(chooseFile, 2, 0);

		gridPane.requestFocus();

		dialog.getDialogPane().setContent(gridPane);
		   
		Optional<String> result = dialog.showAndWait();

		if (result != null && imgPath != null && result.isPresent() && !imgPath.equals("")) {
			Photo photo = new Photo(caption.getText(), imgPath, time);
			album.addPhoto(photo);
			photoList.add(photo);
			Users.write(users);
		}
		
			numberText.setText("Number of Photos: " + album.getAmountOfPhotos());
			oldestText.setText("Oldest Photo: " + 
					(album.getOldestPhoto() == null ? "null" : album.getOldestPhoto().getDateString()));
			rangeText.setText("Date Range: " + album.getRange());

		if(photoList.size() == 1) {
			createButton.setDisable(false);
			tagField.setDisable(false);
			start.setDisable(false);
			end.setDisable(false);
		}
	}
	
	/**
	 * Handles all actions that need to happen when the user clicks on the
	 * search button. This button will search the current photo list for any
	 * current tags that the user enters in the search field text box.
	 * 
	 * 
	 * @param event				Button pressing event
	 * @see ActionEvent
	 */
	public void handleSearchButton(ActionEvent event) {
		if(tagField.getText().isEmpty() && start.getValue() == null && end.getValue() == null) {
			errorDialog("No search parameters");
		} else if(tagField.getText().isEmpty() && start.getValue() != null && end.getValue() == null) {
			errorDialog("No End Date entered");
		} else if(tagField.getText().isEmpty() && start.getValue() == null && end.getValue() != null) {
			errorDialog("No Start Date entered");
		} else if(tagField.getText().isEmpty() && start.getValue() != null && end.getValue() != null && start.getValue().isAfter(end.getValue())) {
			errorDialog("Start Date is after End Date");
		} else if(tagField.getText().isEmpty() && start.getValue() != null && end.getValue() != null && end.getValue().isAfter(start.getValue())) {
			List<Photo> newPhotoList = new ArrayList<Photo>();
			for(Photo p : user.getAlbum(album.getName()).getPhotos()) {
				if(p.inRange(start.getValue(), end.getValue())) {
					newPhotoList.add(p);
				}
			}
			photos = newPhotoList;
			photoList = FXCollections.observableArrayList(newPhotoList);
			list.setItems(photoList);
			list.getSelectionModel().select(0);
		} else if(!tagField.getText().isEmpty() && start.getValue() == null && end.getValue() == null) {
			List<Photo> newPhotoList = new ArrayList<Photo>();
			for(Photo p : user.getAlbum(album.getName()).getPhotos()) {
				if(p.hasTag(tagField.getText())) {
					newPhotoList.add(p);
				}
			}
			photos = newPhotoList;
			photoList = FXCollections.observableArrayList(newPhotoList);
			list.setItems(photoList);
			list.getSelectionModel().select(0);
		} else if(!tagField.getText().isEmpty() && start.getValue() != null && end.getValue() != null) {
			List<Photo> newPhotoList = new ArrayList<Photo>();
			for(Photo p : user.getAlbum(album.getName()).getPhotos()) {
				if(p.hasTag(tagField.getText()) && p.hasTag(tagField.getText())) {
					newPhotoList.add(p);
				}
			}
			photos = newPhotoList;
			photoList = FXCollections.observableArrayList(newPhotoList);
			list.setItems(photoList);
			list.getSelectionModel().select(0);
		}
	}
	
	/**
	 * Handles all actions that need to happen when the user clicks on the
	 * create album from search button. The main purpose is to start/create a
	 * new album with the current photo selected
	 *
	 * @param event				Button pressing event
	 * @see ActionEvent
	 */
	public void handleCreateButton(ActionEvent event) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Create Album From Search");
		dialog.setHeaderText("Create a new album from your search results");
		dialog.setContentText("Album Name: ");
		dialog.setResizable(false);
		   
		Optional<String> result = dialog.showAndWait();
		   			
		if(result.isPresent()) {
			if(user.hasAlbum(result.get())) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Album Already Exists");
				alert.setHeaderText("You already have an album named " + result.get());
				alert.setContentText("Please create a NEW album");
				alert.showAndWait();
			} else {
				Album newAlbum = new Album(result.get(), photos);
				user.addAlbum(newAlbum);
				Users.write(users);
				
				user = users.getUser(user.getUserName());
				album = user.getAlbum(result.get());
				photos = album.getPhotos();
				usernameText.setText("User: " + user);
				albumText.setText("Album: " + album);
				numberText.setText("Number of Photos: " + album.getAmountOfPhotos());
				oldestText.setText("Oldest Photo: " + 
						(album.getOldestPhoto() == null ? "null" : album.getOldestPhoto().getDateString()));
				rangeText.setText("Date Range: " + album.getRange());
				photoList = FXCollections.observableArrayList(album.getPhotos());
				list.setItems(photoList);
				list.getSelectionModel().select(0);
				
				if(photoList.size() == 0) {
					deleteButton.setDisable(true);
					editButton.setDisable(true);
					displayButton.setDisable(true);
					copyButton.setDisable(true);
					moveButton.setDisable(true);
					createButton.setDisable(true);
					tagField.setDisable(true);
					start.setDisable(true);
					end.setDisable(true);
				}
				
				if(user.getAlbums().size() == 1) {
					copyButton.setDisable(true);
					moveButton.setDisable(true);
				}
				
			}
		}
	}
	
	/**
	 * Handles all actions that need to happen when the user clicks on the back
	 * button. This will bring the user back to the album view to see all the
	 * user albums.
	 *
	 * @param event				Button pressing event
	 * @see ActionEvent
	 */
	public void handleBackButton(ActionEvent event) {
		FXMLLoader gui = new FXMLLoader();
		gui.setLocation(getClass().getResource("/view/user.fxml"));

		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		Parent root = null;
		try {
			root = gui.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		UserController controller = gui.getController();
		controller.initialize(user.getUserName());

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle(user.getUserName());
		stage.setResizable(false);
		stage.show();
	}
	
	public void errorDialog(String error) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Search Error");
		alert.setHeaderText("There was an error with searching");
		alert.setContentText(error);
		alert.showAndWait();
	}

}
