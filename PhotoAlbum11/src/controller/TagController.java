package controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import model.Users;

/**
 * Controller for the tags screen, which allows
 * for adding and removing of tags on a photo
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */
public class TagController {

	@FXML ListView<Tag> list;
	@FXML ImageView imageView;

	@FXML Text captionText;
	@FXML Text albumText;
	@FXML Text dateText;
	@FXML Text tagsText;

	@FXML Button addButton;
	@FXML Button deleteButton;
	@FXML Button backButton;

	private Album album;
	private User user;
	private Users users;
	private Photo photo;
	
	private ObservableList<Tag> tagList = FXCollections.observableArrayList();
	String filepath = "data" + File.separator + "userData.dat";
	
	/**
	 * Loads the user and photo being edited
	 * and initializes the tag editing view controller
	 * 
	 * @param userName
	 * 				the name of the current user who is logged in
	 * @param albumName
	 * 				the name of the current album being viewed
	 * @param selected
	 * 				the name of the current photo being edited
	 */
	public void initialize(String userName, String albumName, Photo selected) {
		users = Users.read();
		user = users.getUser(userName);
		album = user.getAlbum(albumName);	
		photo = selected;
		
		tagList = FXCollections.observableArrayList(selected.getTags());
		displayPhoto(photo.getImgPath());
		list.setItems(tagList);
		list.getSelectionModel().select(0);
		
		captionText.setText("Caption: " + photo.getCaption());
		albumText.setText("Album: " + album);
		dateText.setText("Date: " + photo.getDateString());
	}
	
	/**
	 * Handles user action when the add button is clicked. 
	 * Brings up screen to add a tag to the photo
	 * 
	 * @param event			Button pressing event
	 */
	public void handleAddButton(ActionEvent event) {
		Dialog<Tag> dialog = new Dialog<>();
		dialog.setTitle("Add Tag");
	    dialog.setHeaderText("Create a tag for this photo");
	    dialog.setResizable(false);
	   
	    Label type = new Label("Tag Type: ");
		Label value = new Label("Tag Value: ");
		
		TextField typeField = new TextField();		
		TextField valueField = new TextField();
		
		GridPane grid = new GridPane();
		grid.add(type, 1, 1);
		grid.add(typeField, 2, 1);
		grid.add(value, 1, 2);
		grid.add(valueField, 2, 2);
		
		dialog.getDialogPane().setContent(grid);
		
		ButtonType buttonTypeOk = new ButtonType("Ok", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
	
		Optional<Tag> result = dialog.showAndWait();
		
		if(result.isPresent()) {
			String typeText = typeField.getText();
			String valueText = valueField.getText();
			Tag tag = new Tag(typeText, valueText);
			int i = user.getAlbums().indexOf(album);
			int j = album.indexOf(photo);
			photo.addTag(tag);
			album.getPhotos().set(j,photo);
			user.getAlbums().set(i,album);
			tagList.add(tag);
			list.setItems(tagList);
			Users.write(users);
		}
		
	}

	/**
	 * Handles user action when the delete button is clicked. 
	 * Deletes the tag from the photo
	 * 
	 * @param event			Button pressing event
	 */
	public void handleDeleteButton(ActionEvent event) {
		Tag selected = list.getSelectionModel().getSelectedItem();
		int selectedIndex = list.getSelectionModel().getSelectedIndex();
		if(selected != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Delete Tag");
			alert.setHeaderText("Delete " + selected);
			alert.setContentText("Are you sure you want to delete " + selected + "?");
			   
			Optional<ButtonType> result = alert.showAndWait();
			   			
			if(result.get() == ButtonType.OK) {
				int i = user.getAlbums().indexOf(album);
				int j = album.indexOf(photo);
				photo.removeTag(selected);
				album.getPhotos().set(j,photo);
				user.getAlbums().set(i,album);
				tagList.remove(selected);
				list.setItems(tagList);
				Users.write(users);
				if (selectedIndex < tagList.size()) {
					list.getSelectionModel().selectNext();
				}				
			}
		}
	}

	/**
	 * Handles user action when the back button is clicked. 
	 * Returns user to the album screen
	 * 
	 * @param event			Button pressing event
	 */
	public void handleBackButton(ActionEvent event) {
		FXMLLoader gui = new FXMLLoader();
		gui.setLocation(getClass().getResource("/view/album.fxml"));

		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		Parent root = null;
		try {
			root = gui.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		AlbumController controller = gui.getController();
		controller.initialize(user.getUserName(), album.getName());

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.setTitle(album.getName());
		stage.setResizable(false);
		stage.show();
	}

	/**
	 * Displays the photo on ImageView
	 * 
	 * @param filepath 		Filepath for the photo
	 */
	private void displayPhoto(String filepath) {
		File file = new File(filepath);
		String imagepath = null;
		try {
			imagepath = file.toURI().toURL().toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Image imgToShow = new Image(imagepath);
		imageView.setImage(imgToShow);

	}
}
