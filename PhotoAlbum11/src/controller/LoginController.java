package controller;

import java.io.File;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Users;

/**
 * Controller for the login screen, which lets a person
 * login as an admin or one of the created users
 * 
 * @author Patel, Kartik
 * @author Scherer, Nicholas
 *
 */

public class LoginController {

	@FXML Button logInButton;
	@FXML TextField userNameTextField;

	private Users users;
	String filepath = "data" + File.separator + "userData.dat";
	
	/**
	 * Loads the users and initializes the login view controller
	 */
	public void initialize() {
		users = Users.read();
	}
	
	/**
	 * Handles user action when the log in button is clicked. Checks if the user
	 * name is valid, and then will load the user specific screen
	 *
	 * @param event    Button pressing event
	 * @see void
	 */

	public void handleLogInButton(ActionEvent event) {
		String userName = userNameTextField.getText().toLowerCase();
		if (userName.equals("admin")) {
			FXMLLoader gui = new FXMLLoader();
			gui.setLocation(getClass().getResource("/view/admin.fxml"));

			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			Parent root = null;
			try {
				root = gui.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			AdminController controller = gui.getController();
			controller.initialize();
			
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("Administrator");
			stage.setResizable(false);
			stage.show();
		} else if (users != null && users.hasUser(userName)) {
			FXMLLoader gui = new FXMLLoader();
			gui.setLocation(getClass().getResource("/view/user.fxml"));

			Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			Parent root = null;
			try {
				root = gui.load();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			UserController controller = gui.getController();
			controller.initialize(userName);

			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle(userName);
			stage.setResizable(false);
			stage.show();
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Account Not Found");
			alert.setHeaderText("No account with the username '" + userName + "'");
			alert.setContentText("Please enter a valid username or use the 'admin' account.");
			alert.showAndWait();
		}

	}

}